Client
======

Introduction
------------

Client is a graphical interface displayed to authenticate an user.

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/M2UCP/authentication/client.git
