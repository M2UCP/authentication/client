package main

import (
	"./client"
	"fmt"
	"github.com/faiface/pixel/pixelgl"
	"os"
	"time"
)

func run( ) {
	// load configuration
	if configuration, err := client.BuildConfiguration( ); err == nil {
		// build client
		if c, err := client.BuildClient( configuration ); err == nil {
			for c.IsRunning( ) {
				// update client
				c.Update( )

				// sleep
				time.Sleep( time.Second )
			}
		} else {
			fmt.Println( err )
			os.Exit( 1 )
		}
	} else {
		fmt.Println( err )
		os.Exit( 1 )
	}
}

func main( ) {
	pixelgl.Run( run )
}
