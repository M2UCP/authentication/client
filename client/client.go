package client

import (
	"gitlab.com/M2UCP/authentication/authenticator"
	"gitlab.com/M2UCP/authentication/smartcard"
)

type Client struct {
	// GUI
	gui *GUI

	// smart card handler
	smartCardHandler *card.Handler

	// token manager
	tokenManager *authenticator.TokenManager

	// client card handler
	clientCardHandler *ClientCardHandler

	// configuration
	configuration *Configuration

	// server handler
	serverHandler *ServerHandler
}

// build client
func BuildClient( configuration *Configuration ) ( *Client, error ) {
	// allocate
	client := &Client{
		configuration: configuration,
	}

	// build card handler
	var err error
	if client.smartCardHandler, err = card.BuildHandler( ); err != nil {
		return nil, err
	}

	// build token manager
	client.tokenManager = authenticator.BuildTokenManager( client.smartCardHandler,
		authenticator.CardModeM2MClient )

	// build server handler
	client.serverHandler = BuildServerHandler( client.tokenManager,
		configuration )

	// build client card handler
	client.clientCardHandler = BuildCardHandlerClient( client.smartCardHandler )

	// build GUI
	if client.gui, err = BuildGUI( client ); err != nil {
		return nil, err
	}

	// done
	return client, nil
}

// update
func ( client *Client ) Update( ) {
	client.smartCardHandler.Update( )
	client.tokenManager.Update( )
	client.serverHandler.Update( )
}

// is running?
func ( client *Client ) IsRunning( ) bool {
	return client.gui.IsRunning( )
}