package client

import (
	"bytes"
	"fmt"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"gitlab.com/M2UCP/authentication/authenticator"
	"gitlab.com/M2UCP/authentication/smartcard"
	"golang.org/x/image/colornames"
	"golang.org/x/image/font/basicfont"
	"net/http"
	"strconv"
	"time"
	"unicode"
)

const(
	TextDisplayZoom = 2
	DelayBetweenBlink = time.Second
)

type GuiMode int
const(
	GuiModeNotConnectedToServer = GuiMode( iota )
	GuiModeConnectedToServer
	GuiModeFirstLoginStepCard
	GuiModeSecondLoginStepLogin1
	GuiModeSecondLoginStepLogin2
	GuiModeThirdLoginStepBiometrical
)

type GUI struct {
	// mode
	mode GuiMode

	// client
	client *Client
	clientCard *card.Card
	clientCardStatus int // 0 normal, 1 bad card

	// window
	window *pixelgl.Window

	// text atlas
	atlas *text.Atlas

	// typed text
	typedText string

	// blink
	isBlink bool
	lastBlink time.Time

	// login/password
	login string
}

// build GUI
func BuildGUI( client *Client ) ( *GUI, error ) {
	// allocate
	gui := &GUI{
		atlas: text.NewAtlas(
			basicfont.Face7x13,
			text.ASCII,
			text.RangeTable(unicode.Latin) ),
		client: client,
	}

	// build window
	var err error
	if gui.window, err = pixelgl.NewWindow( pixelgl.WindowConfig{
		Bounds: pixel.Rect{
			Min: pixel.Vec{
				X: 0,
				Y: 0,
			},
			Max: pixel.Vec{
				X: 640,
				Y: 480,
			},
		},
		Resizable: true,
		Title: "login",
		VSync: true,
	}); err != nil {
		return nil, err
	}

	// start update thread
	go gui.update( )

	// done
	return gui, nil
}

// is string containing only numeric characters?
func IsStringNumerical( value string ) bool {
	for _, r := range value {
		if !unicode.IsNumber( r ) {
			return false
		}
	}
	return true
}

// update thread
func ( gui *GUI ) update( ) {
	for gui.IsRunning( ) {
		// blink
		if time.Now( ).Sub( gui.lastBlink ) >= DelayBetweenBlink {
			gui.isBlink = !gui.isBlink
			gui.lastBlink = time.Now( )
		}

		// handle status
		switch gui.mode {
			default:
				if !gui.client.serverHandler.isConnected {
					gui.mode = GuiModeNotConnectedToServer
				}
				break

			case GuiModeNotConnectedToServer:
				if gui.client.serverHandler.isConnected {
					gui.mode = GuiModeConnectedToServer
				}
				break
		}

		// handle card
		switch gui.mode {
			default:
				break
			case GuiModeFirstLoginStepCard:
				if gui.clientCard == nil ||
					!gui.clientCard.IsAlive( ) {
					gui.client.smartCardHandler.Lock()
					if lastConnectedCard := gui.client.smartCardHandler.GetLastConnectedCard(); lastConnectedCard != nil {
						// card is client card
						gui.clientCardStatus = 0
						gui.clientCard = lastConnectedCard
					}
					gui.client.smartCardHandler.Unlock()
				}

				if gui.clientCard != nil &&
					len( gui.typedText ) == 4 {
					if gui.window.JustPressed( pixelgl.KeyEnter ) {
						if err := gui.clientCard.TypePin( 1,
							[ ]byte( gui.typedText ) ); err == nil {
							gui.typedText = ""
							// check card mode
							if data, err := gui.clientCard.SendRead( 0x10,
								0x04 ); err == nil {
								if data[ 0 ] == byte( authenticator.CardModeClient ) &&
									data[ 3 ] == byte( authenticator.CardModeClient ) {
									if data, err = gui.clientCard.SendRead( 0x14,
										16 ); err == nil {
										cardID := string( data )
										if data, err = gui.clientCard.SendRead( 0x18,
											16 ); err == nil {
											cardPassword := string( data )

											// notify
											fmt.Println( "[CLIENT GUI] user typed correct PIN, reading cardID(",
												cardID,
												"), cardPassword(",
												cardPassword,
												")" )

											// build body
											body := bytes.NewBuffer( nil )
											body.Write( [ ]byte( "id=" +
												cardID +
												"&password=" +
												cardPassword ) )

											// ask server for data assertion
											request, _ := http.NewRequest( "POST",
												"http://" +
													gui.client.configuration.Server.Hostname +
													":" +
													strconv.Itoa( gui.client.configuration.Server.Port ) +
													"/card",
												body )

											// fill header
											request.Header.Add( authenticator.SessionIDHTTPHeader,
												gui.client.serverHandler.sessionID )
											request.Header.Add( "Content-Type",
												"application/x-www-form-urlencoded" )

											// send request
											if response, err := http.DefaultClient.Do( request ); err == nil {
												_ = response.Body.Close( )
												if response.StatusCode == http.StatusOK {
													gui.mode = GuiModeSecondLoginStepLogin1
													gui.client.serverHandler.OPad = response.Header.Get( authenticator.OPadHTTPHeader )
													gui.client.serverHandler.IPad = response.Header.Get( authenticator.IPadHTTPHeader )
												} else {
													gui.clientCard = nil
													gui.clientCardStatus = 1
												}
											} else {
												gui.clientCard = nil
												gui.clientCardStatus = 1
											}
										}
									}
								} else {
									gui.clientCard = nil
									gui.clientCardStatus = 1
								}
							} else {
								gui.clientCard = nil
								gui.clientCardStatus = 1
							}
						} else {
							fmt.Println( "Code is invalid")
						}
					}
				}
				break

			case GuiModeSecondLoginStepLogin1:
				if gui.window.JustPressed( pixelgl.KeyEnter ) {
					if len( gui.typedText ) > 0 {
						gui.login = gui.typedText
						gui.typedText = ""
						gui.mode = GuiModeSecondLoginStepLogin2
					}
				}
				break
			case GuiModeSecondLoginStepLogin2:
				if gui.window.JustPressed( pixelgl.KeyEnter ) {
					if len( gui.typedText ) > 0 {
						// hash login
						login := authenticator.HashDataDouble( gui.login,
							authenticator.HashPad( gui.client.serverHandler.IPad ),
							authenticator.HashPad( gui.client.serverHandler.OPad ) )
						password := authenticator.HashDataDouble( gui.typedText,
							authenticator.HashPad( gui.client.serverHandler.IPad ),
							authenticator.HashPad( gui.client.serverHandler.OPad ) )

						// build body
						body := bytes.NewBuffer( nil )
						body.Write( [ ]byte( "login=" +
							login +
							"&password=" +
							password ) )

						// ask server for data assertion
						request, _ := http.NewRequest( "POST",
							"http://" +
								gui.client.configuration.Server.Hostname +
								":" +
								strconv.Itoa( gui.client.configuration.Server.Port ) +
								"/login",
							body )

						// fill header
						request.Header.Add( authenticator.SessionIDHTTPHeader,
							gui.client.serverHandler.sessionID )
						request.Header.Add( authenticator.OPadHTTPHeader,
							gui.client.serverHandler.OPad )
						request.Header.Add( authenticator.IPadHTTPHeader,
							gui.client.serverHandler.IPad )
						request.Header.Add( "Content-Type",
							"application/x-www-form-urlencoded" )

						// send request
						if response, err := http.DefaultClient.Do( request ); err == nil {
							_ = response.Body.Close( )
							if response.StatusCode == http.StatusOK {
								gui.clientCardStatus = 0
								fmt.Println( "OK" )
								gui.mode = GuiModeThirdLoginStepBiometrical
							} else {
								gui.clientCardStatus = 1
								gui.typedText = ""
								gui.mode = GuiModeSecondLoginStepLogin1
							}
						}
					}
				}
				break
		}

		// handle character typing
		typedCharacter := gui.window.Typed()
		switch gui.mode {
			default:
				gui.typedText += typedCharacter
				break

			case GuiModeFirstLoginStepCard:
				if gui.clientCard == nil ||
					!gui.clientCard.IsAlive( ) {
					gui.clientCard = nil
					gui.typedText = ""
				} else {
					if IsStringNumerical(typedCharacter) &&
						len(gui.typedText) < 4 {
						gui.typedText += typedCharacter
					}
				}
				break
		}

		// handle character removal
		if gui.window.JustPressed(pixelgl.KeyBackspace) {
			if len(gui.typedText) > 0 {
				gui.typedText = gui.typedText[:len(gui.typedText)-1]
			}
		}

		// handle events
		switch gui.mode {
			case GuiModeConnectedToServer:
				if gui.window.JustPressed( pixelgl.KeyEnter ) {
					gui.mode = GuiModeFirstLoginStepCard
				}
				break
			case GuiModeFirstLoginStepCard:

				break
		}

		// draw window
		gui.drawWindow( gui.window )
	}
}

// update window
func ( gui *GUI ) drawWindow( window *pixelgl.Window ) {
	// clear
	window.Clear(colornames.Black)

	// build text
	t := text.New(pixel.Vec{},
		gui.atlas)

	switch gui.mode {
		case GuiModeConnectedToServer:
			t.Color = colornames.White
			_, _ = fmt.Fprintln( t,
				"M2M authentication is done" )
			t.Color = colornames.Green
			_, _ = fmt.Fprintln(t,
				"\nSession ID:",
				gui.client.serverHandler.sessionID)
			t.Color = colornames.White
			nextKeepAlive := DelayBetweenKeepAlive - time.Now( ).Sub( gui.client.serverHandler.lastKeepAliveTime )
			if nextKeepAlive <= 0 {
				nextKeepAlive = 0
			}
			_, _ = fmt.Fprintln( t,
				"Next keep alive in",
				int( nextKeepAlive / time.Second ),
				"second(s)" )
			t.Color = colornames.Firebrick
			_, _ = fmt.Fprintln( t,
				"\n\nHIT ENTER TO LOGIN" )
			break
		case GuiModeNotConnectedToServer:
			t.Color = colornames.Red
			_, _ = fmt.Fprintln( t,
				"Not connected to server..." )
			if gui.isBlink &&
				!gui.client.serverHandler.tokenManager.IsCardInserted( ) {
				_, _ = fmt.Fprintln( t,
					"Insert your card..." )
			} else {
				_, _ = fmt.Fprintln( t,
					" " )
			}
			break
		case GuiModeFirstLoginStepCard:
			if gui.clientCard == nil {
				t.Color = colornames.White
				if gui.isBlink {
					if gui.clientCardStatus == 0 {
						_, _ = fmt.Fprintln(t,
							"Please insert client smart card...")
					} else {
						_, _ = fmt.Fprintln(t,
							"Previous card was invalid, please insert valid one..." )
					}
				} else {
					_, _ = fmt.Fprintln( t,
						" " )
				}
			} else {
				maskedPin := ""
				for i := 0; i < len( gui.typedText ); i++ {
					maskedPin += "*"
				}
				t.Color = colornames.White
				_, _ = fmt.Fprintln(t,
					"Please type you pin code:",
					maskedPin )
			}
			break

		case GuiModeSecondLoginStepLogin1:
			if gui.clientCardStatus == 1 {
				_, _ = fmt.Fprint( t,
					"login failed, retype ")
			}
			_, _ = fmt.Fprintln( t,
				"Login:",
				gui.typedText )
			break

		case GuiModeSecondLoginStepLogin2:
			maskedText := ""
			for i := 0 ; i < len( gui.typedText ); i++ {
				maskedText += "*"
			}
			_, _ = fmt.Fprintln( t,
				"Password:",
					maskedText )
			break

		case GuiModeThirdLoginStepBiometrical:
			_, _ = fmt.Fprintln( t,
				"INSERT BIO DATA HERE" )
			break
	}

	// center
	position := pixel.Vec{
		X: window.Bounds().Max.X/2 - ((t.Bounds().Max.X-t.Bounds().Min.X)*TextDisplayZoom)/2,
		Y: (window.Bounds().Max.Y-window.Bounds().Min.Y)/2 - ((t.Bounds().Max.Y + t.Bounds().Min.Y) * TextDisplayZoom / 2),
	}

	// draw text
	t.Draw(window,
		pixel.IM.Scaled(t.Orig,
			TextDisplayZoom).Moved(position))

	// update
	window.Update()
}

// is running?
func ( gui *GUI ) IsRunning( ) bool {
	return !gui.window.Closed( )
}