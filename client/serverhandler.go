package client

import (
	"fmt"
	"gitlab.com/M2UCP/authentication/authenticator"
	"net/http"
	"strconv"
	"time"
)

const(
	DelayBetweenKeepAlive = time.Second * 5
)

type ServerHandler struct {
	// configuration
	configuration *Configuration

	// is connected?
	isConnected bool

	// session ID
	sessionID string

	// last keep alive
	lastKeepAliveTime time.Time

	// token manager
	tokenManager *authenticator.TokenManager

	// hash pad
	OPad, IPad, QPad string
}

// build server handler
func BuildServerHandler( tokenManager *authenticator.TokenManager,
	configuration *Configuration ) *ServerHandler {
	// allocate memory
	serverHandler := &ServerHandler{
		tokenManager: tokenManager,
		configuration: configuration,
	}

	// done
	return serverHandler
}

// update server handler
func ( serverHandler *ServerHandler ) Update( ) {
	if serverHandler.isConnected {
		if !serverHandler.tokenManager.IsCardInserted( ) {
			fmt.Println( "[SERVER HANDLER] M2M smart card has been removed, removing session" )
			serverHandler.isConnected = false
		} else {
			if time.Now( ).Sub( serverHandler.lastKeepAliveTime ) >= DelayBetweenKeepAlive {
				// save
				serverHandler.lastKeepAliveTime = time.Now( )

				// notify
				fmt.Println( "[SERVER HANDLER] contacting server for keep alive with session id",
					serverHandler.sessionID )

				// build request
				request, _ := http.NewRequest(  "POST",
					"http://" +
						serverHandler.configuration.Server.Hostname +
						":" +
						strconv.Itoa( serverHandler.configuration.Server.Port ) +
						"/brb",
					nil )

				// add session ID
				request.Header.Add( authenticator.SessionIDHTTPHeader,
					serverHandler.sessionID )

				// send request
				if response, err := http.DefaultClient.Do( request ); err == nil {
					_ = response.Body.Close( )
					if response.StatusCode == http.StatusOK {
						fmt.Println( "[SERVER HANDLER] server accepted keep alive request" )
					} else {
						// notify
						fmt.Println( "[SERVER HANDLER] server didn't accept keep alive request (got",
							response.Status +
							"), removing session",
							serverHandler.sessionID )

						// close session
						serverHandler.isConnected = false
					}
				} else {
					// notify
					fmt.Println( "[SERVER HANDLER] error sending keep alive (",
						err,
						") now removing session",
						serverHandler.sessionID )

					// close session
					serverHandler.isConnected = false
				}
			}
		}
	} else {
		// get token
		if otp, err := serverHandler.tokenManager.GetServerOTP( ); err == nil {
			// notify
			fmt.Println( "[SERVER HANDLER] now trying to contact server with OTP",
				otp )

			// build request
			request, _ := http.NewRequest( "POST",
				"http://" +
					serverHandler.configuration.Server.Hostname +
					":" +
					strconv.Itoa( serverHandler.configuration.Server.Port ) +
					"/hello",
				nil )

			// add OTP
			request.Header.Add( authenticator.OTPHTTPHeader,
				otp )

			// do request
			if response, err := http.DefaultClient.Do( request ); err == nil {
				_ = response.Body.Close( )
				if response.StatusCode == http.StatusOK {
					// extract otp header
					if receivedOtp := response.Header.Get( authenticator.OTPHTTPHeader ); len( receivedOtp ) > 0 {
						// notify
						fmt.Println( "[SERVER HANDLER] server sent OTP",
							receivedOtp )

						// check OTP
						if generatedOtp, err := serverHandler.tokenManager.GetClientOTP( ); err == nil {
							if generatedOtp == receivedOtp {
								// extract hash pad
								serverHandler.OPad = response.Header.Get( authenticator.OPadHTTPHeader )
								serverHandler.IPad = response.Header.Get( authenticator.IPadHTTPHeader )

								// notify
								fmt.Println( "[SERVER HANDLER] server is now trusted, received session ID",
									response.Header.Get( authenticator.SessionIDHTTPHeader ),
									"and OPad/IPad for next login step (" +
									serverHandler.OPad,
									"/",
									serverHandler.IPad +
									")")

								// now connected
								serverHandler.isConnected = true
								serverHandler.sessionID = response.Header.Get( authenticator.SessionIDHTTPHeader )
								serverHandler.lastKeepAliveTime = time.Now( )
							}
						}
					}
				} else {
					// notify
					fmt.Println( "[SERVER HANDLER] got response code ",
						response.Status )
				}
			}
		}
	}
}
