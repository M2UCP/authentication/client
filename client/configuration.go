package client

import (
	"encoding/json"
	"io/ioutil"
)

const(
	ConfigurationFileName = "conf.json"
)

type Configuration struct {
	// server address
	Server struct {
		// hostname
		Hostname string `json:"hostname"`

		// port
		Port int `json:"port"`
	} `json:"server"`
}

// build configuration
func BuildConfiguration( ) ( *Configuration, error ) {
	// allocate
	configuration := &Configuration{ }

	// read file
	if fileContent, err := ioutil.ReadFile( ConfigurationFileName ); err == nil {
		if err = json.Unmarshal( fileContent,
			configuration ); err == nil {
			return configuration, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}