package client

import (
	"gitlab.com/M2UCP/authentication/smartcard"
)

type ClientCardHandler struct {
	// card handler
	cardHandler *card.Handler

	// is active
	// do we activate the module (authentication of client is processing)
	isActive bool
}

// build client card handler
func BuildCardHandlerClient( cardHandler *card.Handler ) *ClientCardHandler {
	// allocate
	clientCardHandler := &ClientCardHandler{
		cardHandler: cardHandler,
	}

	// done
	return clientCardHandler
}

// enable client card processing
func ( clientCardHandler *ClientCardHandler ) EnableModule( ) {

}

// disable client card processing
func ( clientCardHandler *ClientCardHandler ) DisableModule( ) {

}
